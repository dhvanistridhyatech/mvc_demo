<!DOCTYPE html>
<html>
<head>
	<title>form</title>
	
	<link rel="stylesheet" type="text/css" href="form.css">
	<script src="../js/jquery.js"></script>

	<script src="../js/validation.js"></script>
	<style type="text/css">
		input[type=text], select {
		  width: 100%;
		  padding: 12px 20px;
		  margin: 8px 0;
		  display: inline-block;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  box-sizing: border-box;
		}

		input[type=email], select {
		  width: 100%;
		  padding: 12px 20px;
		  margin: 8px 0;
		  display: inline-block;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  box-sizing: border-box;
		}
		input[type=submit] {
		  width: 20%;
		  background-color: #4CAF50;
		  color: white;
		  padding: 14px 20px;
		  margin: 8px 0;
		  border: none;
		  border-radius: 4px;
		  cursor: pointer;
		}

		input[type=submit]:hover {
		  background-color: #45a049;
		}

		div {
		  border-radius: 5px;
		  background-color: #f2f2f2;
		  padding: 5px;

		}
		</style>
</head>

<body>
	
	<?php include 'Mainnav.php';?>
	<div class="main">
		<form action="../controller/registrationController.php" method="post" enctype="multipart/form-data">
		
			<div>
				<div>
				<div class="title">
				Registration Form
				</div>
				</div>
			</div>
		
		<div>
			<div><input type="text" name="firstName" id="firstName" placeholder="First Name" class="textCss"></div>
			<div><p id="fnameError" class="errorCss">Enter First name</p> </div>
		</div>

		<div>
			<div><input type="text" name="lastName" id="lastName" placeholder="Last Name" class="textCss"></div>
			<div><p id="lnameError" class="errorCss">Enter Last name</p> </div>
		</div>



		<div colspan="2">
			<div><input type="radio" id="male" name="gender" value="male" class="radio">male
				<input type="radio" id="female" name="gender" value="female" class="radio">female </div>
			<div> <p id="genderError" class="errorCss">Select gender</p></div>

		</div>

		<div>
			<div><input type="checkbox" class="hobby" name="hobby[]" id="hobby1" value="reading">Reading <input type="checkbox" class="hobby" name="hobby[]" id="hobby2" value="playing">Playing Cricket<input type="checkbox" class="hobby" name="hobby[]" id="hobby3" value="net_surfing">Net Surfing</div>
			<div><p id="hobbyError" class="errorCss">Select atleast one</p></div>

		</div>


		<div>	
			<div><input type="email" name="email" id="email" placeholder="Email" class="textCss"></div>
			<div><p id="emailError" class="errorCss">Enter valid email</p></div>
		</div>


		<div>
			<div><input type="text" name="phoneNumber" id="phoneNumber" placeholder="Phone No" class="textCss"></div>
			<div><p id="phoneNumberError" class="errorCss">Enter valid phone number</p></div>
		</div>
		

		<div>	
			<div><input type="text" name="password" id="password" placeholder="Password" class="textCss"></div>
			<div><p id="passwordError" class="errorCss">Password must contain atleast one special cahracter & one capital letter & one digit & lenth should be 8</p></div>
		</div>

		<div>
			<div><input type="text" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password" class="textCss"></div>
			<div><p id="confirmPasswordError" class="errorCss">Passwords are not same</p></div>
		</div>

		<div>
			<div>Profile Image <input type="file" name="fileImage" id="fileImage" accept="image/*"></div>
			<div><p id="fileError" class="errorCss">Select Image</p></div>
		</div>
			<div><div colspan="2"><center><input type="submit" id="submit" class="submitButton" value="submit"></center> </div>
		</div>
			
		
		</form>
	</div>
</body>
</html>