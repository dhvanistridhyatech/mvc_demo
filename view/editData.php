<!DOCTYPE html>
<html>
<head>
	<title>form</title>
	
	<link rel="stylesheet" type="text/css" href="../view/form.css">
	<script src="../js/jquery.js"></script>

	<script src="../js/validation.js"></script>

</head>
<body>
	<div class="main">
		<form action="../controller/updateController.php" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td colspan="2">
				<div class="title">
				Registration Form
				</div>
				</td>
			</tr>
		<tr>
			<td><input type="text" name="eid" id="eid" class="textCss" value=" <?php echo $i['id']; ?>"></td>
			<td><p id="eidError" class="errorCss">Enter id</p> </td>
		</tr>

		<tr>
			<td><input type="text" name="firstName" id="firstName" class="textCss" value=" <?php echo $i['first_name']; ?>"></td>
			<td><p id="fnameError" class="errorCss">Enter First name</p> </td>
		</tr>

		<tr>
			<td><input type="text" name="lastName" id="lastName" class="textCss" value="<?php echo $i['last_name']; ?>"></td>
			<td><p id="lnameError" class="errorCss">Enter Last name</p> </td>
		</tr>



		<tr>
			<td><input type="radio" id="male" name="gender" value="male" class="radio" <?php if($i['gender']=='male') {?> checked <?php } ?> >male
				<input type="radio" id="female" name="gender" value="female" class="radio" <?php if($i['gender']=='female') {?> checked <?php } ?> >female </td>
			<td> <p id="genderError" class="errorCss">Select gender</p></td>

		</tr>

		<tr>
			<td><?php
				        $focus = explode(",", $i['hobby']);
				   
			 ?>
				<input type="checkbox" class="hobby" name="hobby[]" id="hobby1" value="reading" <?php if(in_array('reading',$focus)){ ?> checked <?php } ?>>Reading 
				<input type="checkbox" class="hobby" name="hobby[]" id="hobby2" value="playing" <?php if(in_array('playing',$focus)){ ?> checked <?php } ?>>Playing Cricket
				<input type="checkbox" class="hobby" name="hobby[]" id="hobby3" value="net_surfing"  <?php if(in_array('net_surfing',$focus)){?> checked <?php }?>>Net Surfing</td>
			<td><p id="hobbyError" class="errorCss">Select atleast one</p></td>

		</tr>


		<tr>	
			<td><input type="email" name="email" id="email" class="textCss" value="<?php echo $i['email']; ?>"></td>
			<td><p id="emailError" class="errorCss">Enter valid email</p></td>
		</tr>


		<tr>
			<td><input type="text" name="phoneNumber" id="phoneNumber" class="textCss" value="<?php echo $i['phone_no']; ?>"></td>
			<td><p id="phoneNumberError" class="errorCss">Enter valid phone number</p></td>
		</tr>
		

		<tr>	
			<td><input type="text" name="password" id="password" class="textCss" value="<?php echo $i['password']; ?>"></td>
			<td><p id="passwordError" class="errorCss">Password must contain atleast one special cahracter & one capital letter & one digit & lenth should be 8</p></td>
		</tr>

		<tr>
			<td><input type="text" name="confirmPassword" id="confirmPassword" class="textCss" placeholder="confirm password"></td>
			<td><p id="confirmPasswordError" class="errorCss">Passwords are not same</p></td>
		</tr>
		<tr>
			<td><img src="../image/<?php echo $i['profile_image']; ?>.jpg"></td>
		</tr>
		<tr>
			<td>Profile Image <input type="file" name="fileImage" id="fileImage" accept="image/*"></td>
			<td><p id="fileError" class="errorCss">Select Image</p></td>
		</tr>
			<tr><td colspan="2"><center><input type="submit" id="submit" class="submitButton" value="submit"></center> </td>
		</tr>
			
		</table>
		</form>
	</div>
</body>
</html>