<?php 
	
?>
	
<?php include 'header.php';?>

<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>
	<style type="text/css">
	        input[type=button]
	         {
		      float: right;
			  width: 10%;
			  background-color: #0000ff7a;
			  color: white;
			  padding: 14px 20px;
			  margin: 8px 0;
			  border: none;
			  border-radius: 4px;
			  cursor: pointer;
           }
	</style>
</head>
<body>
<form action="../view/registrationForm.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
	


	<INPUT TYPE="button" VALUE="Back" onClick="history.go(-1);">
<center><h3>Welcome User!</h3><h2>Showing User Details</h2>
	<table border="1px solid #f2f2f2">
		<tr style="background-color: lightblue;">
			<td>id</td>
			<td>first_name</td>
			<td>last_name</td>
			<td>gender</td>
			<td>hobby</td>
			<td>email</td>
			<td>phone no</td>
			<td>image</td>
			<td>EDIT</td>
			<td>DELETE</td>
		</tr>
		<tr style="background-color: #ffb6c152;">


			<?php foreach ($data as $i) {
			?>
			
			<td><?php echo $i['id']; ?></td>
			<td><?php echo $i['first_name']; ?></td>
			<td><?php echo $i['last_name']; ?></td>
			<td><?php echo $i['gender']; ?></td>
			<td><?php
				        $focus = explode(",", $i['hobby']);
				       foreach($focus as $j)
				       {
				       	echo $j." ";
				       }
			 ?></td>
			<td><?php echo $i['email']; ?></td>
			<td><?php echo $i['phone_no']; ?></td>
			<td><img src="../image/<?php echo $i['profile_image']; ?>.jpg"></td>
			<td><a href="../controller/editController.php?editId=<?php echo $i['id']; ?>">EDIT</a></td>
			<td><a href="../controller/deleteController.php?deleteId=<?php echo $i['id']; ?>">DELETE</a></td>
			
		</tr>
	<?php
		 }
		?>
	</table>

	
	</form>
</body>
</html>
