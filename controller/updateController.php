<?php

	session_start();
	$spassword = isset($_SESSION["spassword"]) ? $_SESSION["spassword"] :"";
			
		if($spassword!="")
		{
			$updateController1 = new updateController();
		}
		else
		{
			header("location:loginController.php");
		}
			

	class updateController 
	{
		
		function __construct()
		{
			
			include '../model/formModel.php';	
			$model =  new formModel();
			$conn = $model -> connection();  
			

		
			 $id1 = $_POST['eid'];
	  	 	 $firstName = (string)$_POST['firstName'];
	   		 $lastName = (string)$_POST['lastName'];
	     	 $gender = (string)$_POST['gender'];
	     	 $hobby=implode(',', $_POST['hobby']);
	     	 $email = (string)$_POST['email'];
	     	 $phoneNo = (string)$_POST['phoneNumber'];
	     	 $password = (string)$_POST['password'];
	     	 $confirmPassword = (string)$_POST['confirmPassword'];
	     	
	     	 $target_dir = "../image/";
			    $target_file = $target_dir . basename($_FILES["fileImage"]["name"]);
			    $uploadOk = 1;
			    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

			    if (move_uploaded_file($_FILES["fileImage"]["tmp_name"], $target_file)) {
			        //echo "The file ". basename( $_FILES["fileImage"]["name"]). " has been uploaded.";
			    } else {
			        echo "Sorry, there was an error uploading your file.";
			    }

			    $image=basename( $_FILES["fileImage"]["name"],".jpg"); // used to store the filename in a variable
 
			
			  
			    $model-> update($id1,$firstName,$lastName,$gender, $hobby, $email,$phoneNo,$password,$image,$conn); 	
			    
				header("location:dashboardController.php");

		}
	}

?>